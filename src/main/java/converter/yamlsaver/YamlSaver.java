package converter.yamlsaver;

import java.nio.file.Path;

public interface YamlSaver {
    Path save(YamlSaveParam yamlSaveParam);
}
