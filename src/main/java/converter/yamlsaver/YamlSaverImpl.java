package converter.yamlsaver;

import org.apache.commons.lang3.Validate;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class YamlSaverImpl implements YamlSaver {

    private final Yaml yaml = new Yaml();

    @Override
    public Path save(YamlSaveParam yamlSaverParam) {
        Validate.notNull(yamlSaverParam, "json output should not be null");
        Path outputFile = yamlSaverParam.outputPath();
        try (Writer bw = Files.newBufferedWriter(outputFile, StandardOpenOption.CREATE)) {
            yaml.dump(yamlSaverParam.yamlMap(), bw);
        } catch (IOException e) {
            throw new YamlSaveException(e, outputFile);
        }
        return outputFile;
    }
}
