package converter.yamlsaver;

import java.io.IOException;
import java.nio.file.Path;

public class YamlSaveException extends RuntimeException {
    public final Path outputFile;

    public YamlSaveException(Throwable cause, Path outputFile) {
        super(cause);
        this.outputFile = outputFile;
    }
}
