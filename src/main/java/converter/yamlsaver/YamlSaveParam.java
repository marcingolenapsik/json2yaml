package converter.yamlsaver;

import com.google.common.collect.ImmutableMap;
import org.immutables.value.Value;

import java.nio.file.Path;

@Value.Immutable
@Value.Style(init = "with*")
public interface YamlSaveParam {
    ImmutableMap<String, Object> yamlMap();
    Path outputPath();
}
