package converter;

import com.google.common.collect.ImmutableMap;
import converter.json2yaml.Converter;
import converter.json2yaml.ConverterImpl;
import converter.yamlsaver.ImmutableYamlSaveParam;
import converter.yamlsaver.YamlSaveException;
import converter.yamlsaver.YamlSaver;
import converter.yamlsaver.YamlSaverImpl;

import java.io.IOException;

public class MainApp {

    public static void main(String[] args) {
        Cli cli = new Cli(args);
        Converter converter = new ConverterImpl();
        YamlSaver saver = new YamlSaverImpl();

        try {
            ImmutableMap<String, Object> yamlMap = converter.convert(cli.sourceFilePath);
            ImmutableYamlSaveParam yamlSaveParam = ImmutableYamlSaveParam.builder()
                    .withYamlMap(yamlMap)
                    .withOutputPath(cli.targetFilePath)
                    .build();

            saver.save(yamlSaveParam);

        } catch (IOException e) {
            throw new RuntimeException("reading exception", e);
        } catch (YamlSaveException e) {
            System.out.println(String.format("Could not save to fallowing path <%s>. Check help below.", e.outputFile));
            cli.printHelp();
        }
    }
}
