package converter.json2yaml;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

final class InputStreamReaderWithClosedProperty extends InputStreamReader {
    private boolean closed = false;

    public InputStreamReaderWithClosedProperty(InputStream inputStream) {
        super(inputStream);
    }

    @Override
    public void close() throws IOException {
        super.close();
        closed = true;
    }

    public boolean isClosed() {
        return closed;
    }
}
