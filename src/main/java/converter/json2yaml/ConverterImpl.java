package converter.json2yaml;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

public class ConverterImpl implements Converter {

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public ImmutableMap<String, Object> convert(Path inputFile) throws IOException {
        try (InputStreamReaderWithClosedProperty isr =
                     new InputStreamReaderWithClosedProperty(Files.newInputStream(inputFile, StandardOpenOption.READ))) {
            return convert(isr);
        }
    }

    @Override
    public ImmutableMap<String, Object> convert(InputStreamReaderWithClosedProperty reader) {
        Validate.notNull(reader, "input stream should not be null");
        Validate.isTrue(!reader.isClosed(), "input stream should not be closed");

        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();

        return ImmutableMap.copyOf((Map<String, Object>) gson.fromJson(reader, type));
    }
}
