package converter.json2yaml;

import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ConverterImplTest {
    public static final Path ACTUAL_PATH = Paths.get("actual.yaml");
    private ConverterImpl converter;

    public ConverterImplTest() {
        this.converter = new ConverterImpl();
    }

    @Test
    public void should_converted_yaml_be_equal_to_expected_json() throws Exception {
        // given
        Path given = Paths.get(ConverterImplTest.class.getResource("given.json").toURI());
        Path expected = Paths.get(ConverterImplTest.class.getResource("expected.yaml").toURI());
        ImmutableMap<String, Object> expectedYamlMap;

        try (InputStream is = Files.newInputStream(expected, StandardOpenOption.READ)) {
            Map<String, Object> fromYaml = (Map<String, Object>)new Yaml().load(is);
            expectedYamlMap = ImmutableMap.copyOf(fromYaml);
        }

        // when
        ImmutableMap<String, Object> actualYamlMap = converter.convert(given);

        // then
        assertThat(actualYamlMap).isEqualTo(expectedYamlMap);
    }

    @After
    public void tearDown() throws Exception {
        Files.deleteIfExists(ACTUAL_PATH);
    }
}